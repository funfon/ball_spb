FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /app

RUN apt-get update && apt-get install -y libpq-dev  \
     gcc \
     postgresql-client

COPY requirements.txt /app/
RUN pip install -r requirements.txt

RUN apt-get autoremove -y gcc

COPY . /app/

#RUN chmod +x /app/docker-entrypoint.sh
#ENTRYPOINT [ "/app/docker-entrypoint.sh" ]
