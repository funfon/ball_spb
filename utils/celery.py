import logging

from yookassa import Payment

from ball.models.payment import EventPayment


def check_pending_payments():
    # Getting list of pending payments from db
    pending_payments = EventPayment.objects.filter(status='pending', yookassa_payment_id__isnull=False)
    logging.info(f'list of pending payment: {pending_payments}')

    for payment in pending_payments:
        # Get yk payment by id
        yk_payment = Payment.find_one(payment.yookassa_payment_id)

        status = yk_payment.status
        logging.info(f'yookassa payment status for payment {payment.id}: {status}')

        if status == 'succeeded':
            payment.status = 'success'
            if payment.event.available_places > 0:
                payment.event.available_places -= 1
                payment.event.save()
                payment.save()
            else:
                # TODO: create refund
                logging.error(f'Refund is needed for payment {payment.id}')

        elif status == 'canceled':
            payment.status = 'cancel'
            payment.save()
