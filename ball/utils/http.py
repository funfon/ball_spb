from typing import Any


def http_response(data: Any = None, error: bool = False, error_text: str = '', additional_errors: dict = None):

    return {
        'data': data,
        'error': error,
        'errorText': error_text,
        'additionalErrors': additional_errors
    }
