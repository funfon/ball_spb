# Type of games for event
SIX = '6x6'
SEVEN = '7x7'
EIGHT = '8x8'
TYPES_OF_GAMES = [
    (SIX, '6x6'),
    (SEVEN, '7x7'),
    (EIGHT, '8x8'),
]


# Payment statuses
PENDING = 'pending'
CANCELED = 'cancel'
AUTHORIZED = 'authorized'
CONFIRMED = 'confirmed'
REFUNDED = 'refunded'
REJECTED = 'rejected'
PAYMENT_STATUSES = [
    (PENDING, 'pending'),
    (CANCELED, 'cancelled'),
    (AUTHORIZED, 'authorized'),
    (CONFIRMED, 'confirmed'),
    (REJECTED, 'rejected'),
    (REFUNDED, 'refunded'),
]

# File Types
IMAGE = 'image'
VIDEO = 'video'
FILE_TYPES = [
    (IMAGE, 'image'),
    (VIDEO, 'video'),
]

# Event statuses
EVENT_OPENED = 'opened'
EVENT_CLOSED = 'closed'
EVENT_FINISHED = 'finished'
EVENT_STATUSES = [
    (EVENT_OPENED, 'opened'),
    (EVENT_CLOSED, 'closed'),
    (EVENT_FINISHED, 'finished'),
]
