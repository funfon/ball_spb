from django.db import models
from django.utils.translation import ugettext as _

from ball.models.user import User
from ball.models.location import Event
from ball.utils.model_choices import PAYMENT_STATUSES, PENDING


class EventPayment(models.Model):
    class Meta:
        db_table = 'event_payments'

    user = models.ForeignKey(User,
                             verbose_name=_('User'),
                             on_delete=models.CASCADE,
                             null=False,
                             related_name='event_payments')
    event = models.ForeignKey(Event,
                              verbose_name=_('Event'),
                              on_delete=models.CASCADE,
                              null=False,
                              related_name='payments'
                              )
    price = models.DecimalField(_('Price'), max_digits=32, decimal_places=2, default=0)
    date = models.DateTimeField(verbose_name=_('Date of a payment'), null=True, auto_now=False)
    status = models.CharField(verbose_name=_('Status of payment'), choices=PAYMENT_STATUSES,
                              default=PENDING, max_length=255)
    currency = models.CharField(verbose_name=_('Currency'), null=False, default='RUB', max_length=5)
    participants = models.PositiveIntegerField(verbose_name=_('Amount of people in payment'), default=1, null=True)

    # yookassa fields
    yookassa_payment_id = models.CharField(verbose_name=_('YK payment id'), null=True, max_length=255)
    yookassa_payment = models.BooleanField(verbose_name=_('Yookassa payment'), null=False, default=False)

    # tinkoff fields
    tnkf_payment_id = models.CharField(verbose_name=_('Tinkoff payment id'), null=True, max_length=255)
    tnkf_payment = models.BooleanField(verbose_name=_('Tinkoff payment'), null=False, default=False)

    def __str__(self):
        return f'{self.user.pk}_{self.event.name}{self.event.pk}'
