from django.db import models
from django.utils.translation import ugettext as _

from ball.models.user import User
from ball.utils.model_choices import FILE_TYPES, IMAGE, EVENT_OPENED, EVENT_STATUSES


class City(models.Model):
    class Meta:
        db_table = 'cities'

    name = models.CharField(verbose_name=_('City name'), null=False, max_length=255, default='unknown')

    def __str__(self):
        return self.name


class SubwayStation(models.Model):
    class Meta:
        db_table = 'subway'

    station_name = models.CharField(verbose_name=_('Station name'), null=False, max_length=255, default='unknown')
    lane_number = models.CharField(verbose_name=_('Lane number'), null=True, max_length=255)
    city = models.ForeignKey(City, verbose_name=_('City'),
                             on_delete=models.CASCADE,
                             null=True,
                             related_name='city')

    def __str__(self):
        return self.station_name


class Format(models.Model):
    class Meta:
        db_table = 'formats'

    name = models.CharField(verbose_name=_('Format'), null=False, max_length=255)

    def __str__(self):
        return self.name


class Location(models.Model):

    class Meta:
        db_table = 'locations'

    name = models.CharField(verbose_name=_('Name'), blank=False, max_length=255, default='unknown')
    address = models.CharField(verbose_name=_('Address'), blank=False, max_length=255, default='unknown')
    lat = models.FloatField(_('Latitude'), null=True, blank=True)
    lng = models.FloatField(_('Longitude'), null=True, blank=True)
    available_formats = models.ManyToManyField(Format, related_name='available_formats')
    subway = models.ForeignKey(SubwayStation, verbose_name=_('Subway'),
                               on_delete=models.CASCADE,
                               null=True,
                               related_name='subway')

    @property
    def map_coordinates(self):
        return list([self.lat, self.lng])

    def __str__(self):
        return self.name


class Event(models.Model):

    class Meta:
        db_table = 'events'

    name = models.CharField(verbose_name=_('Name'), blank=False, max_length=255, default='unknown')
    location = models.ForeignKey(Location, verbose_name=_('Location'),
                                 on_delete=models.CASCADE,
                                 blank=False,
                                 null=False,
                                 related_name='events')
    user = models.ManyToManyField(User, related_name='events')
    available_places = models.IntegerField(verbose_name=_('Available places'), null=True, default=0)
    date = models.DateTimeField(verbose_name=_('Date of the event'), null=True, auto_now=False)
    price = models.DecimalField(_('Price'), max_digits=32, decimal_places=2, default=0)
    format = models.ForeignKey(Format, verbose_name=_('Format'),
                               on_delete=models.CASCADE,
                               blank=True,
                               null=True,
                               related_name='formats')
    event = models.CharField(verbose_name=_('status of the event'), max_length=255, null=False,
                             default=EVENT_OPENED, choices=EVENT_STATUSES)

    def __str__(self):
        return self.name


class EventStatistics(models.Model):
    class Meta:
        db_table = 'event_statistics'

    user = models.ForeignKey(User,
                             verbose_name=_('user'),
                             on_delete=models.CASCADE,
                             null=False,
                             related_name='statistics')
    event = models.ForeignKey(Event,
                              verbose_name=_('event statistic'),
                              on_delete=models.CASCADE,
                              null=True,
                              related_name='event_statistic')

    goals = models.IntegerField(verbose_name=_('amount of goals'), null=False, default=0)
    goals_conceded = models.IntegerField(verbose_name=_('amount of goals conceded'), null=False, default=0)
    assists = models.IntegerField(verbose_name=_('amount of assists'), null=False, default=0)


class LocationMedia(models.Model):
    class Meta:
        db_table = 'location_media'

    location = models.ForeignKey(Location,
                                 verbose_name=_('Location media'),
                                 on_delete=models.CASCADE,
                                 blank=True,
                                 null=True,
                                 related_name='location_media_files')
    file = models.FileField(verbose_name=_('media file'), upload_to='locations', max_length=512)
    type = models.CharField(verbose_name=_('File type'), null=True, choices=FILE_TYPES, default=IMAGE, max_length=255)


class EventMedia(models.Model):
    class Meta:
        db_table = 'event_media'

    event = models.ForeignKey(Event,
                              verbose_name=_('Event media'),
                              on_delete=models.CASCADE,
                              blank=True,
                              null=True,
                              related_name='event_media_files')
    file = models.FileField(verbose_name=_('media file'), upload_to='events', max_length=512)
    type = models.CharField(verbose_name=_('File type'), null=True, choices=FILE_TYPES, default=IMAGE, max_length=255)
