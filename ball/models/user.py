from django.db import models
from django.utils import timezone
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.utils.translation import ugettext as _


class UserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        if not email:
            raise ValueError('Users must have an email address')

        now = timezone.now()
        user = self.model(
            email=self.normalize_email(email),
            is_staff=is_staff,
            is_active=True,
            is_superuser=is_superuser,
            last_login=now,
            joined_at=now,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def get_by_natural_key(self, username):
        return self.get(**{'{}__iexact'.format(self.model.USERNAME_FIELD): username})

    def create_user(self, email, password, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    class Meta:
        db_table = 'users'
        abstract = False

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    email = models.EmailField(_('Email'), max_length=255, unique=True)
    is_staff = models.BooleanField(_('Is staff'), default=False)
    is_active = models.BooleanField(_('Is active'), default=True)
    is_superuser = models.BooleanField(_('Superuser status'), default=False)
    joined_at = models.DateTimeField(_('Joined at'), default=timezone.now)

    first_name = models.CharField(verbose_name=_('first name'), blank=True, null=True, max_length=255, default='')
    last_name = models.CharField(verbose_name=_('last name'), blank=True, null=True, max_length=255, default='')
    nickname = models.CharField(verbose_name=_('nickname'), blank=True, null=True, max_length=255, default='')
    age = models.IntegerField(null=True)
    date_of_birth = models.DateField(verbose_name=_('date of birth'), null=True)
    phone = models.CharField(verbose_name=_('phone number'), blank=True, null=True, max_length=15, default='')

    objects = UserManager()

    def __str__(self):
        if self.last_name and self.first_name:
            return f'{self.last_name} {self.first_name}'
        else:
            return self.email
