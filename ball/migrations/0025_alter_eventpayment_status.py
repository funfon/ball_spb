# Generated by Django 3.2.3 on 2021-06-16 17:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ball', '0024_alter_eventpayment_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventpayment',
            name='status',
            field=models.CharField(choices=[('pending', 'pending'), ('cancel', 'cancelled'), ('authorized', 'authorized'), ('confirmed', 'confirmed'), ('rejected', 'rejected'), ('refunded', 'refunded')], default='pending', max_length=255, verbose_name='Status of payment'),
        ),
    ]
