# Generated by Django 3.2.3 on 2021-06-02 21:35

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ball', '0014_auto_20210602_2032'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserStatistics',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('goals', models.IntegerField(default=0, verbose_name='amount of goals')),
                ('goals_conceded', models.IntegerField(default=0, verbose_name='amount of goals conceded')),
                ('assists', models.IntegerField(default=0, verbose_name='amount of assists')),
                ('total_games', models.IntegerField(default=0, verbose_name='amount of games')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='statistics', to=settings.AUTH_USER_MODEL, verbose_name='user')),
            ],
            options={
                'db_table': 'statistics',
            },
        ),
    ]
