# Generated by Django 3.2.3 on 2021-06-16 02:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ball', '0021_eventpayment_participants'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventpayment',
            name='status',
            field=models.CharField(choices=[('pending', 'pending'), ('cancel', 'cancelled'), ('authorized', 'authorized'), ('confirmed', 'confirmed')], default='pending', max_length=255, verbose_name='Status of payment'),
        ),
    ]
