from celery import shared_task

from utils.celery import check_pending_payments


@shared_task
def payment_check():
    check_pending_payments()
