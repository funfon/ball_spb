from django.urls import path, include
from rest_framework import routers

from ball.views.auth import RegisterView, LogoutView, CurrentView, LoginView
from ball.views.location import LocationListView, AvailableLocationsView
from ball.views.profile import PasswordChangeView, UpdateProfileView
from ball.views.yookassa import PaymentView, SuccessView, CancelView
from ball.views.statistics import UserStatisticsView, StatisticsListView
from ball.views.tinkoff import TNKFPaymentView, TNKFSuccessView, TNKFFailView, TNKFNotifyView

app_name = 'authentication'


profile_router = routers.DefaultRouter()
profile_router.register(prefix=r'update', viewset=UpdateProfileView, basename='update-profile')

urlpatterns = [
    path('location/list', LocationListView.as_view(), name='list'),
    path('location/available', AvailableLocationsView.as_view(), name='available'),

    path('auth/register', RegisterView.as_view(), name='user-register'),
    path('auth/logout', LogoutView.as_view(), name='user-logout'),
    path('auth/login', LoginView.as_view(), name='token-obtain-pair'),
    path('auth/current', CurrentView.as_view(), name='token-refresh'),

    # statistics
    path('statistics/user', UserStatisticsView.as_view(), name='user-statistics'),
    path('statistics/list', StatisticsListView.as_view(), name='all-statistics'),

    # path('profile/update/<int:pk>', UpdateProfileView.as_view(), name='update-profile'),
    path('profile/change-password/<int:pk>', PasswordChangeView.as_view(), name='change-password'),
    path('profile/', include(profile_router.urls)),

    # yookassa
    path('payment/create', PaymentView.as_view(), name='create-payment'),
    path('payment/success', SuccessView.as_view(), name='success-payment'),
    path('payment/cancel', CancelView.as_view(), name='cancel-payment'),

    # tinkoff
    path('payment/tinkoff/create', TNKFPaymentView.as_view(), name='create-tnkf-payment'),
    path('payment/tinkoff/success', TNKFSuccessView.as_view(), name='success-tnkf-payment'),
    path('payment/tinkoff/fail', TNKFFailView.as_view(), name='fail-tnkf-payment'),
    path('payment/tinkoff/notifications', TNKFNotifyView.as_view(), name='tnkf-notifications'),
]
