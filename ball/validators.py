import consts
from django.http.response import JsonResponse
from rest_framework import status
from django.core.exceptions import ValidationError
from ball.models.user import User
from ball.utils.http import http_response


def validate_username(username):
    if User.objects.filter(**{'{}__iexact'.format(User.USERNAME_FIELD): username}).exists():
        # raise ValidationError('User with this {} already exists'.format(User.USERNAME_FIELD))
        return JsonResponse(data=http_response(error=True,
                                               error_text=consts.EMAIL_EXIST), status=status.HTTP_200_OK)
    return username
