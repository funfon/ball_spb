from django.contrib import admin
from django.utils.translation import ugettext as _
from ball.models.user import User
from ball.models.location import EventStatistics, Event, Location, SubwayStation, City, LocationMedia, EventMedia, Format
from ball.models.payment import EventPayment


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'phone',)


class EventStatisticsAdmin(admin.ModelAdmin):
    list_display = ('user', 'goals', 'assists', 'goals_conceded',)

    def get_user(self, obj):
        return f'{obj.user.last_name} {obj.user.first_name}'


class LocationAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'subway',)


class EventAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'location', 'format', 'available_places', 'price', 'date',)


class EventPaymentAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'event', 'price', 'status',)


class SubwayStationAdmin(admin.ModelAdmin):
    list_display = ('id', 'city', 'station_name', 'lane_number',)


class CityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)


class FormatAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)


admin.site.register(User, UserAdmin)
admin.site.register(EventStatistics, EventStatisticsAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(EventPayment, EventPaymentAdmin)
admin.site.register(SubwayStation, SubwayStationAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Format, FormatAdmin)
admin.site.register([LocationMedia])
admin.site.register([EventMedia])
