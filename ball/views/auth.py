import logging

import consts
from django.conf import settings
from django.contrib.auth import login
from django.http import JsonResponse
from rest_framework import views, generics, permissions, status
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from rest_framework_simplejwt.state import token_backend
from ball.serializers.user import UserSerializer
from ball.models.user import User
from ball.utils.http import http_response
from ball.serializers.user import CurrentSerializer, LoginSerializer


class LoginView(TokenObtainPairView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)

        except Exception as e:
            return JsonResponse(data=http_response(error=True, error_text=consts.WRONG_PAYLOAD),
                                status=status.HTTP_200_OK)

        return JsonResponse(data=http_response(data=serializer.validated_data), status=status.HTTP_200_OK)


class CurrentView(TokenRefreshView):
    serializer_class = CurrentSerializer

    def post(self, request, *args, **kwargs):
        try:
            token = request.headers.get('Authorization').split(' ')[1]
        except:
            logging.error('no token in request')
        try:
            token_info = token_backend.decode(token, verify=True)
            user_id = token_info.get('user_id')

            if User.objects.filter(pk=user_id).exists():
                user = User.objects.get(pk=user_id)
                return JsonResponse(data=http_response(data={'user': UserSerializer(user).data}),
                                    status=status.HTTP_200_OK)

            else:
                return JsonResponse(data=http_response(error=True, error_text=consts.USER_DOES_NOT_EXIST),
                                    status=status.HTTP_200_OK)

        except Exception as e:
            serializer = self.get_serializer(data=request.data)

            try:
                serializer.is_valid(raise_exception=True)
                token = serializer.validated_data.get('access')
                token_info = token_backend.decode(token, verify=True)
                user_id = token_info.get('user_id')

                if User.objects.filter(pk=user_id).exists():
                    user = User.objects.get(pk=user_id)

                else:
                    return JsonResponse(data=http_response(error=True, error_text=consts.USER_DOES_NOT_EXIST),
                                        status=status.HTTP_200_OK)

                return JsonResponse(data=http_response(data={'token': serializer.validated_data,
                                                             'user': UserSerializer(user).data}),
                                    status=status.HTTP_200_OK)

            except Exception as e:
                return JsonResponse(data=http_response(error=True, error_text=consts.WRONG_PAYLOAD),
                                    status=status.HTTP_200_OK)


class LogoutView(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()

            return JsonResponse(data=http_response(), status=status.HTTP_200_OK)

        except Exception as e:
            print(e)
            return JsonResponse(data=http_response(error=True, error_text=consts.WRONG_PAYLOAD),
                                status=status.HTTP_200_OK)


class RegisterView(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        try:
            email = request.data.get('email')
            password = request.data.get('password')
            first_name = request.data.get('firstName')
            last_name = request.data.get('lastName')

        except Exception:
            return JsonResponse(data=http_response(error=True, error_text=consts.WRONG_PAYLOAD),
                                status=status.HTTP_200_OK)

        if not User.objects.filter(email=email).exists():
            user = User.objects.create_user(email=email, password=password, first_name=first_name, last_name=last_name)

        else:
            return JsonResponse(data=http_response(error=True, error_text=consts.EMAIL_EXIST),
                                status=status.HTTP_200_OK)

        try:
            user.backend = settings.AUTHENTICATION_BACKENDS[0]

        except Exception:
            return JsonResponse(data=http_response(error=True, error_text=consts.AUTH_BACKEND_ERROR),
                                status=status.HTTP_200_OK)

        login(self.request, user)
        return JsonResponse(http_response(data=UserSerializer(user).data), status=status.HTTP_200_OK)
