import uuid
import datetime
import json
import logging
import requests
import hashlib

import consts

from django.http.response import JsonResponse, HttpResponse, HttpResponseRedirect
from rest_framework import views, status, permissions

from ball.utils.http import http_response
from ball.models.location import Event
from ball.models.payment import EventPayment
from ball.models.user import User
from ball.utils.model_choices import AUTHORIZED, REJECTED, CONFIRMED, REFUNDED, CANCELED


TNKF_API_URL = 'https://securepay.tinkoff.ru/v2'
TNKF_CREATE_PAYMENT_URL = f'{TNKF_API_URL}/Init'
TNKF_CONFIRM_PAYMENT_URL = f'{TNKF_API_URL}/Confirm'
TNKF_CANCEL_PAYMENT_URL = f'{TNKF_API_URL}/Cancel'
TNKF_TEST_TERMINAL_ID = '1623819858604DEMO'
TNKF_TEST_TERMINAL_PW = 'su4g2wdv6mwodc1n'


class TNKFNotifyView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        data = request.data
        logging.info(f'notify view: {data}')

        order_id = data.get('OrderId')

        if order_id:

            if data.get('Success') and data.get('Status') == 'AUTHORIZED':
                logging.info(f'AUTHORIZED')
                resp_status = status.HTTP_200_OK

                try:
                    payment = EventPayment.objects.get(tnkf_payment_id=order_id)
                    payment_id = data.get('PaymentId')
                    logging.info(f'payment: {payment_id}')

                    h = hashlib.sha256()
                    hex_str = f'{TNKF_TEST_TERMINAL_PW}{payment_id}{TNKF_TEST_TERMINAL_ID}'
                    h.update(hex_str.encode())
                    logging.info(f'hex: {h.hexdigest()}')

                    authorized_data = {
                        'TerminalKey': TNKF_TEST_TERMINAL_ID,
                        'PaymentId': payment_id,
                        'Token': h.hexdigest()
                    }
                    logging.info(f'authorized data: {authorized_data}')

                    req = requests.post(url=TNKF_CONFIRM_PAYMENT_URL,
                                        json=authorized_data)

                    logging.info(f'response: {req.status_code}')
                    logging.info(f'response data: {req.text}')

                    if req.status_code == 200:

                        payment.status = AUTHORIZED
                        payment.save()

                    else:
                        logging.error(f'tnkf error, {req.status_code}')
                        resp_status = status.HTTP_400_BAD_REQUEST

                except:
                    logging.error(f'Cant get payment for order id: {order_id}')
                    resp_status = status.HTTP_400_BAD_REQUEST

            elif data.get('Status') == 'CONFIRMED':
                logging.info(f'CONFIRMED')
                resp_status = status.HTTP_200_OK
                try:
                    payment = EventPayment.objects.get(tnkf_payment_id=order_id)
                    logging.info(f'payment: {payment.id}')
                    payment.status = CONFIRMED

                    payment.event.available_places -= payment.participants
                    payment.event.save()
                    payment.save()

                    user = User.objects.get(id=order_id.split('_')[0])
                    user.events.add(payment.event)
                    user.save()

                except Exception as e:
                    logging.error(f'Cant get payment for order id: {order_id}')
                    logging.info(e)
                    resp_status = status.HTTP_400_BAD_REQUEST

            elif data.get('Success') and data.get('Status') == 'REFUNDED':
                logging.info(f'REFUNDED')
                resp_status = status.HTTP_200_OK
                try:
                    payment = EventPayment.objects.get(tnkf_payment_id=order_id)
                    payment_id = data.get('PaymentId')
                    logging.info(f'payment: {payment_id}')

                    payment.status = REFUNDED
                    payment.event.available_places += payment.participants
                    payment.event.save()
                    payment.save()

                    user = User.objects.get(id=order_id.split('_')[0])
                    user.events.remove(payment.event)
                    user.save()

                except:
                    logging.error(f'Cant get payment for order id: {order_id}')
                    resp_status = status.HTTP_400_BAD_REQUEST

            elif data.get('Status') == 'REJECTED':
                logging.info(f'REJECTED')

                resp_status = status.HTTP_200_OK
                payment = EventPayment.objects.get(tnkf_payment_id=order_id)
                logging.info(f'payment: {payment.id}')
                payment.status = REJECTED
                payment.save()

            else:
                logging.info('unknown status of payment')
                resp_status = status.HTTP_400_BAD_REQUEST

        else:
            logging.info('no order id')
            resp_status = status.HTTP_400_BAD_REQUEST

        logging.info(resp_status)
        logging.info('\n\n\n')
        return HttpResponse("OK", status=resp_status, content_type='text/csv')


class TNKFSuccessView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        data = request.query_params
        logging.info(f'succes view: {data}')

        order_id = data.get('OrderId')

        try:
            payment = EventPayment.objects.get(tnkf_payment_id=order_id)
            logging.info(f'payment: {payment.id}')

            payment.event.available_places -= payment.participants
            payment.save()

            user = User.objects.get(id=order_id.split('_')[0])
            user.events.add(payment.event)
            user.save()

        except Exception as e:
            # TODO cancel payment
            logging.error(e)
            pass

        logging.info('\n\n\n')
        return HttpResponseRedirect(redirect_to='https://mosfoot.ru')


class TNKFFailView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        data = request.query_params
        logging.info(f'fail view: {data}')

        order_id = data.get('OrderId')
        payment_id = data.get('PaymentId')

        try:
            payment = EventPayment.objects.get(tnkf_payment_id=order_id)
            logging.info(f'payment: {payment.id}')

            h = hashlib.sha256()
            hex_str = f'{TNKF_TEST_TERMINAL_PW}{payment_id}{TNKF_TEST_TERMINAL_ID}'
            h.update(hex_str.encode())
            logging.info(f'hex: {h.hexdigest()}')

            cancel_data = {
                'TerminalKey': TNKF_TEST_TERMINAL_ID,
                'PaymentId': payment_id,
                'Token': h.hexdigest()
            }
            logging.info(f'cancel payment data: {cancel_data}')

            req = requests.post(url=TNKF_CANCEL_PAYMENT_URL,
                                json=cancel_data)
            logging.info(f'response: {req.status_code}')
            logging.info(f'response data: {req.text}')

            if req.status_code == 200:

                payment.status = CANCELED
                payment.save()

        except Exception as e:
            logging.error(e)

        logging.info('\n\n\n')
        return HttpResponseRedirect(redirect_to='https://mosfoot.ru')


class TNKFPaymentView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        """
        required params for response:
            :event: int, event id
            :user: dict, user data
            :participants: int, amount of participants in payment
        """

        # getting data from request
        req_uuid = uuid.uuid4()
        data = request.data
        logging.info(f'create payment view: {data}')
        user = request.user
        event_id = data.get('event')
        participants = data.get('participants', 1)

        if request.user.is_anonymous:
            user_data = data.get('user')
            email = user_data.get('email', '')

            if User.objects.filter(email=email).exists():
                user = User.objects.get(email=email)
            else:
                user = User(email=email, phone=user_data.get('phone', ''))
                user.save()

        logging.info(f'user: {user.id}')

        # If data is empty return error
        if not event_id or not user:
            logging.error('Wrong payload for payment')
            return JsonResponse(http_response(error=True, error_text=consts.WRONG_PAYLOAD), status=status.HTTP_200_OK)

        event = Event.objects.get(pk=event_id)
        logging.info(f'event: {event.id}')

        if event.available_places - participants < 0:
            logging.error(f'not enough places in event, {event.available_places} < {participants}')
            return JsonResponse(http_response(error=True, error_text=consts.NOT_ENOUGH_PLACES),
                                status=status.HTTP_200_OK)

        amount = int(event.price) * participants * 100
        order_id = f'{user.id}_{event.id}_{req_uuid}'

        h = hashlib.sha256()
        hex_str = f'{amount}{event.name}{order_id}{TNKF_TEST_TERMINAL_PW}{TNKF_TEST_TERMINAL_ID}'
        h.update(hex_str.encode())
        logging.info(f'hex: {h.hexdigest()}')

        req = requests.post(url=TNKF_CREATE_PAYMENT_URL,
                            json={
                                'TerminalKey': TNKF_TEST_TERMINAL_ID,
                                'Amount': amount,
                                'OrderId': order_id,
                                'Description': event.name,
                                'Token': h.hexdigest()
                            })

        logging.info(f'response: {req.status_code}')
        logging.info(f'response: {req.text}')

        if req.status_code == 200:
            resp = json.loads(req.text)
            confirmation_url = resp.get('PaymentURL')
            logging.info(f'confirmation url: {confirmation_url}')

        else:
            logging.error(f'tnkf error, {req.status_code}')
            return JsonResponse(http_response(error=True, error_text=consts.PAYMENT_FAILED), status=status.HTTP_200_OK)

        event_payment = EventPayment(
            user=user,
            event=event,
            price=event.price,
            date=datetime.datetime.now(),
            tnkf_payment=True,
            tnkf_payment_id=f'{user.id}_{event.id}_{req_uuid}',
            participants=participants,
        )
        event_payment.save()

        logging.info('\n\n\n')
        return JsonResponse(http_response(data={'redirect_url': confirmation_url}), status=status.HTTP_200_OK)
