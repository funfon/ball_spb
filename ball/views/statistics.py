import consts
import datetime

from django.http import JsonResponse
from rest_framework import views, status, permissions

from ball.serializers.user import StatisticSerializer
from ball.models.location import EventStatistics
from ball.utils.http import http_response
from ball.models.user import User


class StatisticsListView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        date_from = request.query_params.get('date_from')
        date_to = request.query_params.get('date_to')

        statistics = EventStatistics.objects.all()
        users = User.objects.all()

        if date_from:
            date_from = datetime.datetime.strptime(date_from, '%d/%m/%Y')
            statistics = statistics.filter(event__date__gte=date_from)

        if date_to:
            date_to = datetime.datetime.strptime(date_to, '%d/%m/%Y')
            statistics = statistics.filter(event__date__lte=date_to)

        serializer = StatisticSerializer(statistics, many=True)
        result_statistic = list()

        for user in users:

            goals = 0
            assists = 0
            games = 0
            goals_conceded = 0

            for stat in serializer.data:
                if user.id == stat.get('user'):
                    goals += stat.get('goals')
                    assists += stat.get('assists')
                    goals_conceded += stat.get('goals_conceded')
                    games += 1

            result_statistic.append({
                'user_id': user.id,
                'user_name': f'{user.first_name} {user.last_name}',
                'goals': goals,
                'assists': assists,
                'goals_conceded': goals_conceded,
                'games': games,
            })

        return JsonResponse(http_response(data=result_statistic), status=status.HTTP_200_OK)


class UserStatisticsView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):

        user_id = request.query_params.get('user_id', None)

        if not user_id:
            if request.user.is_anonymous:
                return JsonResponse(http_response(error=True, error_text=consts.PERMISSION_DENIED),
                                    status=status.HTTP_200_OK)
            else:
                user = request.user

        else:
            user = User.objects.get(id=user_id)

        date_from = request.query_params.get('date_from')
        date_to = request.query_params.get('date_to')

        statistics = EventStatistics.objects.filter(user=user)

        if date_from:
            date_from = datetime.datetime.strptime(date_from, '%d/%m/%Y')
            statistics = statistics.filter(event__date__gte=date_from)

        if date_to:
            date_to = datetime.datetime.strptime(date_to, '%d/%m/%Y')
            statistics = statistics.filter(event__date__lte=date_to)

        serializer = StatisticSerializer(statistics, many=True)

        goals = 0
        assists = 0
        games = 0
        goals_conceded = 0

        for stat in serializer.data:
            goals += stat.get('goals')
            assists += stat.get('assists')
            goals_conceded += stat.get('goals_conceded')
            games += 1

        result_statistic = {
            'user_id': user.id,
            'user_name': f'{user.first_name} {user.last_name}',
            'goals': goals,
            'assists': assists,
            'goals_conceded': goals_conceded,
            'games': games,
        }

        return JsonResponse(http_response(data=result_statistic), status=status.HTTP_200_OK)
