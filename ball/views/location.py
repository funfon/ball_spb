import consts
import datetime
import logging
from django.http import JsonResponse
from rest_framework import views, status, permissions

from ball.models.location import Event, Location
from ball.serializers.location import LocationListSerializer
from ball.utils.http import http_response
from ball.utils.model_choices import IMAGE, VIDEO

logger = logging.getLogger(__name__)


class LocationListView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        locations = Location.objects.all()
        # response = dict()
        serializer = LocationListSerializer(locations, many=True)

        # for location in locations:
        #     data = {
        #         'name': location.name or '',
        #         'address': location.address or '',
        #         'mapCoordinates': [location.lat, location.lng] or [],
        #         'price': location.price or None,
        #         'media': location.location_media_files or None
        #     }
        #
        #     response.update({location.pk: data})

        return JsonResponse(http_response(data=serializer.data), status=status.HTTP_200_OK)


class AvailableLocationsView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):

        date_from = request.query_params.get('date_from')
        date_to = request.query_params.get('date_to')

        if date_from:
            date_from = datetime.datetime.strptime(date_from, '%d/%m/%Y')

        else:
            return JsonResponse(data=http_response(error=True, error_text=consts.WRONG_PAYLOAD),
                                status=status.HTTP_200_OK)

        try:
            date_to = datetime.datetime.strptime(date_to, '%d/%m/%Y')

        except TypeError:
            date_to = datetime.datetime.today()

        daily_locations = dict()

        while date_to >= date_from:

            locations = Location.objects.all()
            available_locations_result = list()

            for location in locations:
                events = list()
                if date_from:
                    events = Event.objects.filter(date__date=date_from.strftime('%Y-%m-%d'), location=location)

                for event in events:
                    images = list()
                    video = list()

                    for file in event.event_media_files.filter(type=IMAGE):
                        images.append(str(file.file))

                    for file in event.event_media_files.filter(type=VIDEO):
                        video.append(str(file.file))

                    available_locations_result.append({'locationId': location.pk,
                                                       'eventId': event.id,
                                                       'availableCount': event.available_places,
                                                       'price': event.price,
                                                       'format': event.format.name,
                                                       'name': event.name,
                                                       'date': event.date,
                                                       'eventMediaFiles': {
                                                           'video': video,
                                                           'images': images
                                                       }})

            daily_locations.update({date_from.strftime('%d/%m/%Y'): available_locations_result})

            date_from = date_from + datetime.timedelta(days=1)

        return JsonResponse(http_response(data=daily_locations), status=status.HTTP_200_OK)
