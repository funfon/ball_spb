import os
import datetime
import logging

import consts
import uuid

from django.http.response import JsonResponse, HttpResponse
from rest_framework import views, status
from yookassa import Configuration, Payment

from ball.utils.http import http_response
from ball.models.location import Event
from ball.models.payment import EventPayment
from ball.models.user import User

Configuration.configure(os.environ.get('YOOKASSA_SHOP_ID'), os.environ.get('YOOKASSA_SECRET_KEY'))


class SuccessView(views.APIView):

    def post(self, request):
        data = request.data
        payment_id = data.get('object').get('id')
        payment = EventPayment.objects.get(yookassa_payment_id=payment_id)
        payment.status = 'success'
        payment.save()

        return HttpResponse(status=status.HTTP_200_OK)


class CancelView(views.APIView):

    def post(self, request):
        data = request.data
        payment_id = data.get('object').get('id')
        payment = EventPayment.objects.get(yookassa_payment_id=payment_id)
        payment.status = 'cancel'
        payment.save()

        return HttpResponse(status=status.HTTP_200_OK)


class PaymentView(views.APIView):

    def post(self, request, *args, **kwargs):
        """
        required params for response:
            :amount: str
        """
        idempotence_key = str(uuid.uuid4())

        # getting data from request
        data = request.data
        user = request.user
        event = data.get('event')

        if request.user.is_anonymous:
            user_data = data.get('user')
            email = user_data.get('email', '')
            try:
                if User.objects.filter(email=email).exists():
                    user = User.objects.get(email=email)
                else:
                    user = User(email=email, phone=user_data.get('phone', ''))
                    user.save()

            except:
                return JsonResponse(http_response(error=True, error_text=consts.WRONG_PAYLOAD),
                                    status=status.HTTP_200_OK)

        # If data is empty return error
        if not event or not user:
            return JsonResponse(http_response(error=True, error_text=consts.WRONG_PAYLOAD), status=status.HTTP_200_OK)

        event = Event.objects.get(pk=event.get('id'))

        payment = Payment.create({
            "amount": {
                "value": event.location.price,
                "currency": "RUB"
            },
            "confirmation": {
                "type": "redirect",
                "return_url": os.environ.get('YOOKASSA_REDIRECT_URL', 'https://mosfoot.ru')
            },
            "capture": True,
            "description": "Заказ №2"
        }, idempotence_key)

        confirmation_url = payment.confirmation.confirmation_url

        event_payment = EventPayment(
            user=user,
            event=event,
            price=payment.amount.value,
            date=datetime.datetime.now(),
            yookassa_payment_id=payment.id,
            yookassa_payment=True
        )
        event_payment.save()

        return JsonResponse(http_response(data={'redirect_url': confirmation_url}), status=status.HTTP_200_OK)
