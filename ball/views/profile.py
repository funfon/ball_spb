from django.http.response import JsonResponse
from rest_framework import views, status, permissions, viewsets

import consts
from ball.utils.http import http_response
from ball.models.user import User
from ball.serializers.profile import UpdateProfileSerializer


class ObtainUserMixin:
    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            return JsonResponse(http_response(error=True, error_text=consts.USER_DOES_NOT_EXIST),
                                status=status.HTTP_200_OK)


class UpdateProfileView(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        return User.objects.filter(pk=self.request.user.pk)

    def update(self, request, *args, **kwargs):
        try:
            instance = self.get_object()

        except Exception as e:
            return JsonResponse(http_response(error=True, error_text=consts.PERMISSION_DENIED),
                                status=status.HTTP_200_OK)

        serializer = UpdateProfileSerializer(instance, data=request.data, partial=True)

        if not serializer.is_valid(raise_exception=False):
            return JsonResponse(http_response(error=True, error_text=consts.WRONG_PAYLOAD),
                                status=status.HTTP_200_OK)
        self.perform_update(serializer)

        return JsonResponse(http_response({'user': serializer.data}), status=status.HTTP_200_OK)


class PasswordChangeView(views.APIView, ObtainUserMixin):
    permission_classes = (permissions.IsAuthenticated,)

    def put(self, request, pk):
        instance = self.get_object(pk)
        user = request.user

        if user.pk != instance.pk:
            return JsonResponse(http_response(error=True, error_text=consts.PERMISSION_DENIED),
                                status=status.HTTP_200_OK)

        if instance.check_password(request.data.get('old_password', None)):
            instance.set_password(request.data.get('password', None))
            instance.save()
            return JsonResponse(http_response(), status=status.HTTP_200_OK)
        else:
            return JsonResponse(http_response(error=True, error_text=consts.WRONG_PAYLOAD), status=status.HTTP_200_OK)
