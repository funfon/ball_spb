from rest_framework import serializers

from ball.models.user import User


class UpdateProfileSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=False)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'nickname', 'age', 'date_of_birth', 'phone')


# class UpdateProfileSerializer(serializers.ModelSerializer):
#     email = serializers.EmailField(required=False)
#
#     class Meta:
#         model = User
#         fields = ('first_name', 'last_name', 'email', 'nickname', 'age', 'date_of_birth', 'phone')
#
#     def update(self, instance, validated_data):
#         user = self.context['request'].user
#
#         if user.pk != instance.pk:
#             raise serializers.ValidationError({"authorize": "You dont have permission for this user."})
#
#         instance.first_name = validated_data['first_name']
#         instance.last_name = validated_data['last_name']
#         instance.email = validated_data['email']
#         instance.nickname = validated_data['nickname']
#         instance.age = validated_data['age']
#         instance.date_of_birth = validated_data['date_of_birth']
#         instance.phone = validated_data['phone']
#
#         instance.save()
#
#         return instance

