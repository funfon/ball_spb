from rest_framework import serializers

from ball.models.location import Location
from ball.utils.model_choices import IMAGE, VIDEO


class LocationListSerializer(serializers.ModelSerializer):
    map_coordinates = serializers.ReadOnlyField()
    location_media_files = serializers.SerializerMethodField()
    available_formats = serializers.SerializerMethodField()
    subway = serializers.SerializerMethodField()
    city = serializers.SerializerMethodField()

    class Meta:
        model = Location
        fields = ('id', 'name', 'address', 'map_coordinates', 'subway', 'city', 'available_formats',
                  'location_media_files',)

    def get_subway(self, obj):
        return obj.subway.station_name

    def get_city(self, obj):
        return obj.subway.city.name

    def get_location_media_files(self, obj):
        images = list()
        video = list()
        for file in obj.location_media_files.filter(type=IMAGE):
            images.append(str(file.file))

        for file in obj.location_media_files.filter(type=VIDEO):
            video.append(str(file.file))

        return {'video': video, 'images': images}

    def get_available_formats(self, obj):
        result = list()
        for fmt in obj.available_formats.all():
            result.append(fmt.name)
        return result


class AvailableLocationInfo(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ('id', 'available_places',)
