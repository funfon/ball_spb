from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer, TokenRefreshSerializer

from ball.models.user import User
from ball.models.location import EventStatistics


class StatisticSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()

    class Meta:
        model = EventStatistics
        fields = ('user', 'goals', 'goals_conceded', 'assists',)

    def get_user(self, obj):
        return obj.user.id


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'last_login',
            'email',
            'first_name',
            'last_name',
            'age',
            'date_of_birth',
            'phone',
            'joined_at',
        )
        read_only_fields = ('last_login', 'joined_at')


class LoginSerializer(TokenObtainPairSerializer):

    def validate(self, attrs):
        data = super(LoginSerializer, self).validate(attrs)
        user = {
            'id': self.user.id,
            'email': self.user.email,
            'last_login': self.user.last_login,
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'age': self.user.age,
            'date_of_birth': self.user.date_of_birth,
            'phone': self.user.phone,
            'joined_at': self.user.joined_at,
        }
        data.update({'user': user})
        return data


class CurrentSerializer(TokenRefreshSerializer):
    pass

    def validate(self, attrs):
        data = super().validate(attrs)
        # user = {
        #     'email': self.user.email,
        #     'last_login': self.user.last_login,
        #     'first_name': self.user.first_name,
        #     'last_name': self.user.last_name,
        #     'age': self.user.age,
        #     'date_of_birth': self.user.date_of_birth,
        #     'phone': self.user.phone,
        #     'joined_at': self.user.joined_at,
        # }
        # data.update({'user': user})
        return data
