#!/bin/bash
python manage.py flush --no-input
python manage.py collectstatic --no-input

echo "Migrate the Database at startup of project"

# Wait for few minute and run db migraiton
while ! python manage.py migrate  2>&1; do
   echo "Migration is in progress status"
   sleep 3
done

echo "Django docker is fully configured successfully."

exec "$@"

bash -c "python /app/manage.py migrate --no-input && python manage.py collectstatic --no-input && python /app/manage.py runserver 0.0.0.0:8000"
